-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Wrz 2019, 12:46
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `baza`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `ID` int(10) NOT NULL,
  `Nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`ID`, `Nazwa`, `Adres`, `Telefon`, `NIP`) VALUES
(1, 'Angelo ', 'Poznań', '123456789', ''),
(2, 'Dante', 'Kraków', '234567891', ''),
(3, 'Dąb', 'Wrocław', '345678912', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `ID` int(10) NOT NULL,
  `Alias` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `Ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `Kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `Kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`ID`, `Alias`, `Ulica`, `Miasto`, `Kod`, `Kraj`) VALUES
(1, 'Mały', 'ul.Długa', 'Gdańsk', '74-300', 'Polska'),
(2, 'Duży', 'ul.Małego', 'Poznań', '74-110', 'Polska'),
(3, 'Średni', 'ul.Dźwigowa', 'Warszawa', '01-365', 'Polska'),
(4, 'Mały', 'ul.Średnia', 'Gdynia', '74-301', 'Polska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `ID` int(10) NOT NULL,
  `Artykuł` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Opis` text COLLATE utf8_polish_ci NOT NULL,
  `Kod_produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `Cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`ID`, `Artykuł`, `Opis`, `Kod_produktu`, `Cena`) VALUES
(1, 'Klawiatura ', 'Klawiatura jest w stanie wywalenia do kosza.', '2378239491030584', 13),
(2, 'Myszka', 'Myszka działająca tylko uważaj na wejście usb. ', '2324658790126723', 18),
(3, 'Monitor', 'Monitor działa ale sam zaiwentujesz w okablowanie', '1256750393183849', 12),
(4, 'Karta Graficzna', 'Karta działa ale nie pozwalać innym jej ruszać.', '3402028342930352', 21),
(5, 'Kabel do zasilacza ', ' Kabel jest różnej długości i nie zawsze będzie działał.', '4398125494039373', 15),
(6, 'Dysk', 'Dyski są różnej pamięci ale kabel do podłączenia kupisz sobie sam.\r\n', '6482910343084522', 30),
(7, 'Windows 7', 'Oprogramowanie zadziała , niedługo nie będzie wspomagany.', '0978238923671278', 50),
(8, 'Karta Dzwiękowa', '', '9023782390901278', 40),
(9, 'Procesor', '', '9015327371854627', 87),
(10, 'Pendrive', 'Pendrive będzie działał ale uważajcie na uważajcie na wejście USB. ', '2089327823923531', 25);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID` int(10) NOT NULL,
  `ID_produktu` int(10) NOT NULL,
  `ID_magazyn` int(10) NOT NULL,
  `Ilosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID`, `ID_produktu`, `ID_magazyn`, `Ilosc`) VALUES
(1, 6, 1, 250),
(2, 8, 2, 200),
(3, 10, 3, 500),
(4, 2, 4, 67),
(5, 7, 1, 100),
(6, 4, 2, 150),
(7, 5, 3, 100),
(8, 1, 4, 180),
(9, 3, 1, 345),
(10, 9, 2, 170);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_produktu` (`ID_produktu`),
  ADD KEY `ID_magazyn` (`ID_magazyn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD CONSTRAINT `stany_ibfk_1` FOREIGN KEY (`ID_magazyn`) REFERENCES `magazyn` (`ID`),
  ADD CONSTRAINT `stany_ibfk_2` FOREIGN KEY (`ID_produktu`) REFERENCES `produkty` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
