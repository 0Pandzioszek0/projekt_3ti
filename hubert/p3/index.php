<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Sklep papierniczy</title>
    <link rel="Stylesheet" type="text/css" href="styl.css">
</head>
<body>
    <div id="baner">
        <h1>W naszym sklepie internetowym kupisz najtaniej</h1>
    </div>
    <div id="lewy">
        <h3>Promocja 15% obejmuje artykuły</h3>
        <ul></ul>
    </div>
    <div id="srodkowy">
        <h3>Cena wybranego artykułu w promocji</h3>
        <select name="">
            <option>Gumka do mazania</option>
            <option>Cienkopis</option>
            <option>Pisaki 60 szt.</option>
            <option>Markery 4 szt.</option>
        </select>
        <input type="button" value="WYBIERZ"> 
    </div>
    <div id="prawy">
        <h3>Kontakt</h3>
        <p>telefon: 123123123</p>
        <p>e-mail:<a href="bok@sklep.pl"></a></p>
        <img src="promocja2.png" alt="promocja">
    </div>
    <div id="stopka">
        <h4>Autor strony: 00000000000</h4>
    </div>
</body>
</html>