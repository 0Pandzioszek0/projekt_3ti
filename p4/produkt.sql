-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 30 Maj 2019, 09:02
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sklep`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(11) NOT NULL,
  `referencja` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `nazwa` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `cena` float NOT NULL,
  `ilosc` int(11) NOT NULL,
  `obrazek` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `opis` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`id`, `referencja`, `nazwa`, `cena`, `ilosc`, `obrazek`, `opis`) VALUES
(1, 'mop1', 'mopopies 3001', 120, 20, 'https://www.dravenstales.ch/wp-content/uploads/2016/03/hunde-wischmopp-01.jpg', 'Mopopies umyje ci podłogi lepiej niż grażyna szmatką! najnowszy hit sezonu! To właśnie Mopopies 3001! zainwestuj i ciesz się jego małą praktycznością już dziś\r\n! '),
(2, 'mop2', 'Kapcie Teściowej', 56, 17, 'https://ae01.alicdn.com/kf/HTB1Isz3PVXXXXXeXVXXq6xXFXXXm.jpg', 'Idealny prezent dla teściowej. Może i wykonanie Chińskie ale mamusia będzie zachwycona! do wyboru wiele pięknych kolorów !'),
(3, 'reklamowka1', 'super torba 1250', 1556, 100, 'https://www.e-worki.pl/sklep/wp-content/uploads/2016/11/4ade5f196f4877f02c59f171ce92a4a3.jpg', 'pojemna! unikalny wzór! ciekawy sposób przechowywania rzeczy! Proszę Państwa! oto reklamówka! hit w tej kategorii pod względem braku wytrzymałości! '),
(4, 'reklamowka2', 'Damska torebka', 34, 23, 'http://paknysa.pl/galerie/r/reklamowka-ucho-38x44-25_323.jpg', 'Niezwykła dokładność w detalach, piękny i nietuzinkowy kwiatowy motyw, tak! damska torebka nigdy nie wyglądała tak nowocześnie a zarazem zjawiskowo! Piękniej nie będzie!'),
(5, 'mop', 'Latający spodek', 1356, 234, 'https://www.sklep.vileda.pl/img/0b9ee92e_large.jpg', 'Latający spodek to mop dzięki któremu mycie podłóg nigdy nie będzie takie same!'),
(6, 'reklamowka', 'MĘSKA TORBA 1421!', 44444, 23, 'https://frega24.pl/media/catalog/product/cache/1/image/800x600/c562ae099580d6a062289618533acacb/F/R/FREGA_518661_1_1.jpg', 'Męska torebka dla każdego janusza biznesu! somsiady patrzą z zadrością na najnowszy produkt naszej marki!'),
(7, 'reklamowka', 'eko torba', 123, 54, 'https://www.lumex.pl/cache/crop-800-800/aHR0cHM6Ly93d3cubHVtZXgucGwvZmlsZXMvb3Bha293YW5pYS1mb2xpb3dlL3Jla2xhbW93a2EtZWtvLWtvbmljenluYS5qcGc=.jpg', 'EKO reklamówka! to idealna reklamówka dla tych co chcą dostać się do eko grupy! z nią na pewno zaimponujesz swą wielkodusznością każdemu'),
(8, 'mop', 'Mop 1233344', 5000, 1, 'https://images.morele.net/news/i700/9846.png', 'Mop obślini ci podłogę i nigdy więcej nie będziesz musiał myć jej ty! Prócz tego zajmuje się też utylizacją butów i kapeluszy.');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
