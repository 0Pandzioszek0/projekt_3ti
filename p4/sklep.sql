-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 24 Maj 2019, 11:06
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sklep`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawa`
--

CREATE TABLE `dostawa` (
  `id` int(11) NOT NULL,
  `nazwa` text NOT NULL,
  `koszt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id` int(11) NOT NULL,
  `imie` text NOT NULL,
  `nazwisko` text NOT NULL,
  `adres` text NOT NULL,
  `telefon` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `koszyk`
--

CREATE TABLE `koszyk` (
  `id` int(11) NOT NULL,
  `idKlient` int(11) NOT NULL,
  `idProdukt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `platnosci`
--

CREATE TABLE `platnosci` (
  `id` int(11) NOT NULL,
  `nazwa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(11) NOT NULL,
  `referencja` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `nazwa` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `cena` float NOT NULL,
  `ilosc` int(11) NOT NULL,
  `obrazek` text CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `opis` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`id`, `referencja`, `nazwa`, `cena`, `ilosc`, `obrazek`, `opis`) VALUES
(1, 'mop1', 'mopopies 3001', 120, 20, 'https://www.dravenstales.ch/wp-content/uploads/2016/03/hunde-wischmopp-01.jpg', 'Mopopies umyje ci podłogi lepiej niż grażyna szmatką! najnowszy hit sezonu! To właśnie Mopopies 3001! zainwestuj i ciesz się jego małą praktycznością już dziś\r\n! '),
(2, 'mop2', 'Kapcie Teściowej', 56, 17, 'https://ae01.alicdn.com/kf/HTB1Isz3PVXXXXXeXVXXq6xXFXXXm.jpg', 'Idealny prezent dla teściowej. Może i wykonanie Chińskie ale mamusia będzie zachwycona! do wyboru wiele pięknych kolorów !'),
(3, 'reklamowka1', 'super torba 1250', 1556, 100, 'https://www.e-worki.pl/sklep/wp-content/uploads/2016/11/4ade5f196f4877f02c59f171ce92a4a3.jpg', 'pojemna! unikalny wzór! ciekawy sposób przechowywania rzeczy! Proszę Państwa! oto reklamówka! hit w tej kategorii pod względem braku wytrzymałości! '),
(4, 'reklamowka2', 'Damska torebka', 34, 23, 'http://paknysa.pl/galerie/r/reklamowka-ucho-38x44-25_323.jpg', 'Niezwykła dokładność w detalach, piękny i nietuzinkowy kwiatowy motyw, tak! damska torebka nigdy nie wyglądała tak nowocześnie a zarazem zjawiskowo! Piękniej nie będzie!');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienie`
--

CREATE TABLE `zamowienie` (
  `id` int(11) NOT NULL,
  `idKlient` int(11) NOT NULL,
  `wartosc` float NOT NULL,
  `idKoszyk` int(11) NOT NULL,
  `idDostawa` int(11) NOT NULL,
  `idPlatnosci` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
