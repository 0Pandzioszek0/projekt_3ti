-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 27 Wrz 2019, 11:08
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `baza`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `id` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `nip` varchar(12) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`id`, `nazwa`, `adres`, `telefon`, `nip`) VALUES
(1, 'Waneko Sp. z o.o.', 'Trzcinowa 27 Warszawa', '505693323', '5213016370'),
(2, 'Studio JG sp. z o.o.', 'Staniewicka 18 Warszawa', '664279966', '5252607750'),
(3, 'Wydawnictwo KOTORI Beata Bamber', 'Sowia 7 Gołuski', '', '5341061666'),
(4, 'J.P.Fantastica Shin Yasuda', 'Podmiejska 2a Mierzyn', '914833199', '8471256074'),
(5, 'Dango Martyna Raszka ', 'Rosnowo 2/2', '', '4990599669');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `id` int(10) NOT NULL,
  `alias` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`id`, `alias`, `ulica`, `miasto`, `kod`, `kraj`) VALUES
(1, 'Zbyszek', 'Dąbrowskiego 12', 'Szczecin', '666666', 'Polska'),
(2, 'Grażyna', 'Legionów polskich 39b', 'Wrocław', '152612', 'Polska'),
(3, 'Seba', 'Piłsudskiego 18/35', 'Warszawa', '192008', 'Polska'),
(4, 'Yennefer', 'Geralta 2', 'Łódź', '200710', 'Polska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `id` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `specyfikacja` text COLLATE utf8_polish_ci DEFAULT NULL,
  `kod_produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`id`, `nazwa`, `specyfikacja`, `kod_produktu`, `cena`) VALUES
(2, 'Bakemonogatari tom 01 ', 'Choć Araragi Koyomi to z pozoru zwykły uczeń liceum, w rzeczywistości jego życie dalekie jest od normalności. Świeżo wyleczony z wampiryzmu, wciąż odczuwa skutki uboczne ugryzienia. W dodatku wszystko wskazuje na to, że zaczął przyciągać do siebie ludzi mających problemy nieziemskiej natury. Pewnego dnia po lekcjach ratuje przed upadkiem Hitagi Senjōgaharę, koleżankę z klasy, która potknęła się na schodach. Kiedy łapie ją w swoje ramiona orientuje się, że dziewczyna... nic nie waży! W dodatku zamiast okazać mu wdzięczność, uzbrojona po zęby, w z pozoru niewinne, a jednak ostre akcesoria biurowe, Hitagi atakuje go i szantażuje, aby mieć pewność, że ten nie zdradzi nikomu jej sekretu. Zamiast uciekać w popłochu Araragi postanawia jej pomóc. Wspólnie udają się do opuszczonego budynku, gdzie mieszka tajemniczy człowiek, który lepiej niż ktokolwiek inny zna się na nadnaturalnych przypadłościach...', '4190', 21.3),
(3, 'Re:Zero - Życie w innym świecie od zera (LN) #01', 'Wracając z supermarketu, Subaru Natsuki, licealista jakich wielu, zostaje nagle przywołany do innego świata. Znowu przeniosło kogoś do świata fantasy?! Jednak na miejscu nie ma śladu po osobie, która go ściągnęła, a sam główny bohater dość szybko pakuje się w kłopoty i zostaje napadnięty przez rabusiów. Z opresji ratuje go tajemnicza srebrnowłosa piękność i jej duchowy pomocnik, z wyglądu przypominający kota. Pragnąc się odwdzięczyć, Subaru obiecuje pomoc w odzyskaniu pewnej skradzionej rzeczy. Kiedy już mają złapać złodzieja, zostają zaatakowani i giną... A przynajmniej powinni zginąć. Subaru odzyskuje przytomność w miejscu, w które został przeniesiony ze swojego świata. \"Powrót przez śmierć\", czyli restart po śmierci okazuje się jedyną nową zdolnością, jaką został obdarzony. Dzięki niej pragnie rozwiązać nawet najbardziej beznadziejne sytuacje i uratować poznaną niedawno dziewczynę.', '3143', 21.5),
(4, 'Bleach tom 01 ', 'Ichigo Kurosaki, lat 15. Szczególne uzdolnienia: widzi duchy. Poza tym prowadzi względnie normalne życie w spokojnym miasteczku Karakura. Pewnego dnia jednak spotyka dziwną dziewczynę w czarnym kimonie, która określa siebie mianem strażniczki śmierci. Wkrótce rodzina Ichigo zostaje zaatakowana przez potwora zwanego hollow! Czy nasz bohater zdoła ich uratować!?', '173', 13.5),
(5, 'Gdy zapłaczą cykady tom 01 - Księga uprowadzenia przez demony.', 'Przed Wami adaptacja komiksowa \"Księgi uprowadzenia przez demony\" - prologu opowieści z serii\r\n\"Gdy zapłaczą cykady\".\r\n\r\n„Nie wszystko jest takie piękne, na jakie wygląda…” Keiichi Maebara, który właśnie przeprowadził się do niewielkiej wioski\r\no nazwie Hinamizawa, nawet nie wie, jak bardzo prawdziwe jest to krótkie zdanie… Na początku wszystko w jego nowym\r\nżyciu wydaje się być w jak najlepszym porządku: wszyscy są dla niego mili, szybko zdobywa nowych przyjaciół w szkole,\r\nlecz sielanka kończy się w momencie, w którym chłopak dowiaduje się, że jego nowy dom skrywa mroczną tajemnicę…\r\n\r\nPierwsze cztery księgi \"Gdy zapłaczą cykady\" to:\r\n-Księga uprowadzenia przez demony\r\n-Księga dryfującej bawełny\r\n-Księga morderczej klątwy\r\n-Księga marnowania czasu\r\nWszystkie księgi w wydaniu 2w1.', '2246', 23.75),
(6, 'No Game No Life tom 01 - LN', 'Sora i Shiro to rodzeństwo genialnych gamerów, o których krążą w internecie miejskie legendy. W prawdziwym życiu są parą NEETów i hikikomori, a otaczającą ich rzeczywistość nazywają beznadziejną grą. Pewnego dnia tajemniczy Bóg przenosi ich do swojego świata, gdzie wojna jest surowo zabroniona, a wszystkie spory rozwiązywane są przy pomocy gier. Tak, nawet te dotyczące granic państw. W miejscu zdominowanym przez inne nacje ludzie stoją na krawędzi zagłady, jedno miast jest wszystkim, co im pozostało. Czy Sora i Shiro, bezużyteczni członkowie społeczeństwa, zdołają ich ocalić? Niech rozpocznie się gra!', '2561', 21.5),
(7, 'Beztroski kemping tom 01 ', 'Rin - dziewczyna, która biwakuje nad brzegiem jeziora pod Górą Fuji. Nadeshiko - dziewczyna, która przyjechała na rowerze, by móc podziwiać najpiękniejszy szczyt Japonii. Jedząc ramen w kubku, cieszą się niesamowitą scenerią... Jaki krajobraz odbija się w ich oczach? Lektura tej mangi sprawi, że sami zechcecie pojechać na kemping! Jeżeli nawet nigdzie się nie ruszycie, to ten tytuł pozwoli Wam poczuć się jakbyście byli na biwaku! Przed wami świeże spojrzenie na hobby na łonie natury!', '4050', 19.9),
(8, 'Służąca Przewodnicząca tom 01', 'Misaki, przewodnicząca samorządu szkolnego w do niedawna męskim, a od niedawna koedukacyjnym liceum Seika, dzień w dzień walczy z egoizmem i ignorancją uczniów płci brzydszej. Dziewczyna skrywa jednak pewien sekret. Po godzinach dorabia sobie, pracując w kawiarni jako... udająca służącą kelnerka! Pewnego dnia o wszystkim dowiaduje się Usui, arcyprzystojny i arcypopularny uczeń Seiki. Czy to oznacza, że już niebawem cała szkoła dowie się o podwójnym życiu Misaki!?', '111', 18),
(9, 'Death Note tom 01 ', 'Pewnego dnia demon Ryuk zgubił w świecie ludzi notes zwany \"Death Note\". Kto mógł przypuszczać, że to z pozoru niewinne wydarzenie stanie się początkiem wielkiego starcia pomiędzy dwoma wybrańcami – geniuszem Lightem Yagami oraz tajemniczym detektywem o pseudonimie L...! Panie i panowie - czas na największy pojedynek wszechczasów!', '432', 16),
(10, 'Sword Art Online tom 01 ', 'Dopóki gra nie zostanie ukończona, opuszczenie jej uniwersum jest niemożliwe, a wirtualna śmierć równa się śmierci w rzeczywistości. Dziesięć tysięcy graczy, nie znając prawdy o tajemniczej grze Sword Art Online, loguje się do jej świata, a wtedy rozpoczyna się brutalna gra o przetrwanie.\r\n\r\nJeden z użytkowników uwięzionych w SAO, Kirito, bardzo szybko akceptuje prawdziwe oblicze gry. Wybiera drogę samotnego wojownika, by zdobywać kolejne kondygnacje stupiętrowego, dryfującego w przestrzeni zamku Aincrad. W drodze na szczyt w pojedynkę walczy z potworami i pokonuje wszystkie przeciwności losu. Pewnego dnia jednak zostaje zmuszony przez Asunę, mistrzynię rapiera, do zawiązania z nią drużyny. To spotkanie okaże się brzemienne w skutki i wciągnie Kirito w wir nieprzewidzianych zdarzeń.\r\n\r\nOto nadchodzi legendarna powieść, która na stronie internetowej autora pochłonęła już ponad sześć i pół miliona czytelników!', '1632', 21),
(11, 'twoje imię. tom 01 ', 'Mitsuha mieszka w malutkim miasteczku, marząc o tym, aby kiedyś rozpocząć studia w stolicy. Taki mieszka i uczy się w Tokio. Pewnego razu, na wskutek przedziwnego fenomenu wywołanego przelatującą w pobliżu Ziemi kometą, na jeden dzień zamieniają się ciałami. To, co z początku wydawało im się snem, wkrótce okazuje się ich nową rzeczywistością, ponieważ zjawisko powtarza się za każdym razem, gdy zasypiają. Aby przetrwać ten zwariowany okres muszą obmyślić sposób, aby się ze sobą komunikować. Zwłaszcza że wkrótce będą musieli zmierzyć się z wydarzeniem, które wszystko zmieni...\r\n\r\nFilm \"Kimi no na wa\" to jeden z najbardziej dochodowych japońskich filmów wszech czasów. To pełna uroku i humoru, trzymająca w napięciu historia, którą docenili fani na całym świecie. Odkryj ją na nowo w formie komiksu!', '3498', 21);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `id` int(10) NOT NULL,
  `id_produktu` int(10) NOT NULL,
  `id_magazynu` int(10) NOT NULL,
  `ilosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`id`, `id_produktu`, `id_magazynu`, `ilosc`) VALUES
(1, 2, 1, 100),
(2, 7, 3, 50),
(3, 4, 2, 10),
(4, 9, 4, 500),
(5, 5, 2, 66),
(6, 6, 3, 31),
(7, 3, 1, 729),
(8, 10, 3, 99),
(9, 8, 2, 573),
(10, 11, 4, 612);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produktu` (`id_produktu`),
  ADD KEY `id_magazynu` (`id_magazynu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD CONSTRAINT `stany_ibfk_1` FOREIGN KEY (`id_produktu`) REFERENCES `produkt` (`id`),
  ADD CONSTRAINT `stany_ibfk_2` FOREIGN KEY (`id_magazynu`) REFERENCES `magazyn` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
